Making heater strips
===================

## BOM list

 * A fabric ribbom, of the desired width and length.

 * A female Velcro strip, of the desired width and length.

 * A small piece of male Velcro.

 * A elastic ribbom, about 1/3 of the length of the heater strip to make.

 * Heater wire. You must calculate the tolal length and number of turns.
   Try to use flexible and thin one.

     * https://es.aliexpress.com/item/1000001203553.html

     * https://es.aliexpress.com/item/1000001820322.html

 * Black silicone 704.

     * https://es.aliexpress.com/item/4000844247860.html

 * Wire of 4 threads 24AWG.

     * https://es.aliexpress.com/item/4000023467303.html

 * Thermistor NTC NTCLE203E3103FB0 (10K, Nominal temp. 25, B 3977).

## Built step by step

### Step 1

Sew a piece of male Velcro to the elastic ribbon and the edge of the elastic
ribbon to the fabric base ribbon.

### Step 2

Fix the base ribbon using nails.
![nail fixation](media/02-heater_strips-nail_fixation.jpg)

### Step 3

Place the heater wire in position.
![heater placement](media/03-heater_strips-heater_placement.jpg)

### Step 4

Cover the heater wire with the silicone. Avoid to do this in the extremes.
![silicone coat](media/04-heater_strips-silicone_coat.jpg)

### Step 5

Glue over the silicone the female Velcro.
![velcro placement](media/05-heater_strips-velcro_placement.jpg)

### Step 6

After cured, remove the nails and cover and glue with silicone the extreme
(tail) of strip. Only the extreme of the strip where we will make the
connections must be now unglued.
![before connections](media/06-heater_strips-before_connections.jpg)

### Step 7

Connect the main wire to the heater wire and to the thermistor. Note that
a part of the plastic cover of the main wire is sewed to the base fabric
(for strengthen).
![connections detail](media/07-heater_strips-connections_detail.jpg)

### Step 8

Cover and glue with silicone the part of the strip where the connections are.

Sew all the edges of the strip for strengthen and best look.
![reinforcement stitching](media/08-heater_strips-reinforcement_stitching.jpg)

### Step 9

Put the GX12 (or whatever) connector.
![connectors detail](media/09-heater_strips-connectors_detail.jpg)

Here the pinout:
#### Connectors pinout

![GX12 female pinout](media/GX12_female.jpg)

