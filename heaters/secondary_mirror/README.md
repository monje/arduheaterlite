Making secondary (mirror and support) heaters
=============================================

Heater frame support for secondary mirror heater element.

Print with TPU (flexible).

Cut by the middle of the long thick block in the major axis for allow to place
in site.

The heater wire is plazed en a zig-zag pattern using the internal wire guides

A wire guide is missing for allow space to make the conections.

A hole in one of the wire guide allow put the thermal sensor (thermistor).


## BOM

 * Wire for spider.

     * https://es.rs-online.com/web/p/conductores-de-interconexion-cables-para-equipos/8015035/

 * Wire for secondary cage 22AWG black.

     * https://es.aliexpress.com/item/1005002138009919.html

 * Wire for secondary support: 24AWG black.

     * https://es.aliexpress.com/item/1005002138009919.html

 * Black masking tape 15mm (no adhesive residue).

     * https://www.amazon.es/gp/product/B08N63532J/

 * Black masking tape 20mm (no adhesive residue).

     * https://www.amazon.es/gp/product/B08N63TN6W/

 * Double sided adhesive tape (no adhesive residue).

     * https://es.aliexpress.com/item/4000296635940.html

 * Connectors.

     *  https://es.aliexpress.com/item/32815992024.html

 * 3mm bridle.

 * 704 sealer.

     * https://es.aliexpress.com/item/1005001771527307.html

 * Heater wire

 * Black fine fabric

 * Black insulating cloth (felt)

## Built step by step (secondary mirror heater)

### Step 1

Print 3D heater frame support for secondary mirror heater element.

Print with TPU (flexible).

Cut by the middle of the long thick block in the major axis for allow to place
in site.

### Step 2

Place heater wire. Fix with 704 sealer.
![place wire](media/02-secondary_mirror_heater.jpg)

### Step 3

Prepare thermistor.
![prepare thermistor](media/03-secondary_mirror_heater_sensor.jpg)

### Step 4

Put thermistor and prepare terminals
![put thermistor](media/04-secondary_mirror_heater.jpg)

### Step 5

Put connector
![put connector](media/05-secondary_mirror_heater_connector.jpg)

### Step 6

Put adhesive
![put connector](media/06-secondary_mirror_heater_adhesive.jpg)

### Step 7

Stick
![stick](media/07-secondary_mirror_heater_onsite.jpg)

### Step 8

Wired
![wired](media/08-secondary_mirror_heater_wired.jpg)


## Built step by step (secondary support heater)

Prepare a paper template that suit your secondary support.

### Step 1

Put the heater wire on black fine fabric. Fix with 704 sealer.

![put wire](media/01-secondary_support_heater.jpg)

### Step 2

Put black insulating cloth over all. Fix with 704 sealer and sew.

Note the hole for the reminals. This heater has no thermistor.

![put wire](media/02-secondary_support_heater.jpg)

### Step 3

Use a velcro strip for fixation in site.

![put wire](media/03-secondary_support_heater.jpg)

### Step 4

Put connector.

