//////////////////////////////////////////////////////
// Heater frame support for secondary mirror heater
// element.
// Print with TPU (flexible).
// Cut by the middle of the long thick block in the
// major axis (see docs).
//
// See docs for details.
/////////////////////////////////////////////////////

// (C) Ruben Diez-Lazaro 2020

// PARAMETERS FOR CUSTOM DESIGN
// The minor axis of the secondary mirror:
minoraxisout=69;
// The minor axis of the secondary mirror support:
minoraxisin=56;
// shift
shift=1.5;
// Thickness of the base of the heater frame (floor):
// (Better a multiplier of printer layer height)
h_base=0.30;
// Total height of the heater frame:
h_total=2.4;
// Thickness of edgew:
wall_thickness=1.2;
//turn diameter
turn_dia=2.8;
// Put a guide every .... degrees
turn_step=8;
// Diameter of the hole for the sensor:
sensor_hole=3.8;

//
// YOU SHOULD NOT MODIFY ANYTHING FROM HERE
// UNLESS YOU KNOW WHAT YOU ARE DOING
//
$fn=100;

// Make a "skew circular" plain ring.
module skew_ring(axisout,axisin)
{
    scale([1/sqrt(2),1,1])
difference(){
difference(){
    difference(){
      rotate(-45,[0,1,0]) cylinder(r=axisout/2,h=sqrt(2)*axisout,center=true);
      rotate(-45,[0,1,0]) translate([-shift,0,-1])cylinder(r=axisin/2,h=sqrt(2)*axisout+2,center=true);
    }
    translate([0,0,-axisout])cube(size = [2*axisout,2*axisout,2*axisout], center = true);
}
  translate([0,0,axisout+h_total])cube(size = [2*axisout,2*axisout,2*axisout], center = true);
}
    }

// Make the heater frame skeleton
module mainframe(axisout,axisin)
{
  difference(){
      skew_ring(axisout,axisin);
      translate([0,0,h_base]) skew_ring(axisout-wall_thickness,axisin+wall_thickness);
  }
}

// Make the internal supports:
module turn(angle){
    translate([minoraxisin/2+(minoraxisout-minoraxisin)/4-shift*cos(angle),0,0])
    cylinder(r=(cos(angle)*shift/2+turn_dia)/2,h=h_total);
}


// Make all except the sensor hole
module all(){
union(){
mainframe(minoraxisout, minoraxisin);

for(i=[turn_step/2+turn_step:turn_step:360-2*turn_step]){
rotate([0,0,i])turn(i);
}

translate([minoraxisin/2-shift,-1.5*wall_thickness,0])cube([(minoraxisout-minoraxisin)/2+shift/2,3*wall_thickness,h_total]);
}
}

// Do all, including the hole for the sensor
module allall(){
difference(){
all();
angle=3.5*turn_step;
rotate([0,0,-angle])translate([minoraxisin/2+(minoraxisout-minoraxisin)/4-shift*cos(angle),0,h_base])scale([0.6,1,1])cylinder(r=sensor_hole/2,h=h_total);
}
}
scale([sqrt(2),1,1]) allall();