//////////////////////////////////////////////////////
// Heater frame support for secondary mirror heater
// element.
// Print with TPU (flexible).
// Cut by the middle of the long thick block in the
// major axis (see docs).
//
// See docs for details.
/////////////////////////////////////////////////////

// (C) Ruben Diez-Lazaro 2020

// PARAMETERS FOR CUSTOM DESIGN
// The minor axis of the secondary mirror:
minoraxisout=69;
// The minor axis of the secondary mirror support:
minoraxisin=56;
// shift
shift=-0.6;
// Thickness of the base of the heater frame (floor):
// (Better a multiplier of printer layer height)
h_base=0.30;
// Total height of the heater frame:
h_total=2.4;
// Thickness of edgew:
wall_thickness=1.2;

//
// YOU SHOULD NOT MODIFY ANYTHING FROM HERE
// UNLESS YOU KNOW WHAT YOU ARE DOING
//
$fn=100;

// Make a "skew circular" plain ring.
module skew_ring(axisout,axisin)
{
    scale([1/sqrt(2),1,1])
difference(){
difference(){
    difference(){
      rotate(-45,[0,1,0]) cylinder(r=axisout/2,h=sqrt(2)*axisout,center=true);
      rotate(-45,[0,1,0]) translate([-shift,0,-1])cylinder(r=axisin/2,h=sqrt(2)*axisout+2,center=true);
    }
    translate([0,0,-axisout])cube(size = [2*axisout,2*axisout,2*axisout], center = true);
}
  translate([0,0,axisout+h_total])cube(size = [2*axisout,2*axisout,2*axisout], center = true);
}
    }

// Make the heater frame skeleton
module mainframe(axisout,axisin)
{
  difference(){
      skew_ring(axisout,axisin);
      translate([0,0,h_base]) skew_ring(axisout-wall_thickness,axisin+wall_thickness);
  }
}

// Make all
module all(){
union(){
mainframe(minoraxisout, minoraxisin);

translate([minoraxisin/2-shift,-1.5*wall_thickness,0])cube([(minoraxisout-minoraxisin)/2+shift/2,3*wall_thickness,h_total]);
}
}

scale([sqrt(2),1,1]) all();