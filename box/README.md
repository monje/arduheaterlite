ArduHeaterLite box
==================

3D printable box for Lite version of ArduHeater:
https://github.com/jbrazio/arduheater

You can find here these 3D models:

 * arduheaterlite-box.stl: the box

 * arduheaterlite-cap.stl: the cap for the box

 * arduheaterlite-auxsupport.stl: an auxuliar support for put the box over a
   curved area

In the src folder you can find the FreeCAD source code for these models.
In particular, the arduheaterlite-auxsupport design is made for fix my
telescope, so you probably want to modify it according your needs.

## BOM list

Apart of the 3D printed models, you also will need:

 * SWITCH 15x10mm (x1)

     * https://es.aliexpress.com/item/33024631441.html

     * https://es.aliexpress.com/item/4000121837373.html

 * FUSE HOLDER 5x20mm (x1)

     * https://es.aliexpress.com/item/32864479515.html
   
 * POWER CONNECTOR 5.5x2.5mm (FEMALE) WITH DUST CAP (x1)

     * https://es.aliexpress.com/item/4000555711683.html

 * RED LED 3mm (x5)

     * https://es.aliexpress.com/item/1005001465625933.html
   
 * LED HOLDER 3mm (x5)

     * https://es.aliexpress.com/item/1005001387902992.html
  
 * GX12 CONNECTOR 4P (FEMALE) WITH DUST CAP (x4)

     * https://es.aliexpress.com/item/32955106065.html

 * SCREW M2.6x8 (x4)

     * https://es.aliexpress.com/item/32975410255.html

 * WIRE

 * A KIND OF FASTON CONNECTOR THAT MATCH ONE OF THE SWITCH TERMINAL (x1)

 * A KIND OF FASTON CONNECTOR THAT MATCH ONE OF THE FUSE HOLDER (x1)

 * DUPONT CONNECTOR 4P (FEMALE) (x4)

 * DUPONT CONNECTOR 2P (FEMALE) (x4)

 * A 5x20mm FUSE (recomended 4A)

## Constructions

Print the 3D models. If needed, remember modify the "arduheaterlite-auxsupport"
for your case.

I recommend use ABS for better durability.

Make the connections as showed in this picture (please note the two type faston 
connector in the end of the wires of the switch):
![box connectors](media/box_connectors.jpg)

And in this picture you can see a general view of the box with all mounted in
it.
![box mount](media/box_mount.jpg)

### Connectors pinout

![dupont 4p pinout](media/dupont_4p.jpg)

![GX12 male pinout](media/GX12_male.jpg)

