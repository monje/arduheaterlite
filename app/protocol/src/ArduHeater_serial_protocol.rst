==========================
ARDUHEATER serial protocol
==========================
**DOC VERSION: 20191213**

General considerations
=======================
There is two types of commands

 - The command that acts over a channel. These coomands have the generic form
   "*XN*", where "*X*" is a letter and "*N*" is the channel number, from 0 to 3.

 - The command that don't need specify a channel. These commands have the form
   "*X*", where is a letter or character.

In the case of the commands that returns configurable parameters, you can use
the same command followed by a coma separate list of the new desired
paramentes.

The returned information is write in the form "*:XX<LIST_OF_VALUES>#*", where
"*XX*" is the command (1 or 2 characters).

Commands
========

$
~~
It has no parameters.

Prints a help message. Currentelly return no really util information.

V
~~
It has no parameters.

Prints the firmware version.

Example:

| send:
| ``V``
| receive:
| ``:V0.4a#``

?
~~
It has no parameters.

Outputs status for a channel. The status string is in the form
"*is_connected(channel_0), is_connected(channel_1),is_connected(channel_2),
is_connected(channel_3),is_ready(channel_0),is_ready(channel_1),
is_ready(channel_2),is_ready(channel_3),is_enabled(chnnel_0),
is_enabled(chnnel_1),is_enabled(chnnel_2),is_enabled(chnnel_3)*".

Example:

| send:
| ``?``
| receive:
| ``:?1,0,0,0,1,0,0,0,0,0,0,0#``

This command does not support reconfigure parameters.

+
~~
It has channel ID as parameter. No output.

Enables a channel.

Example:

| send:
| ``+0``

You can consult the new state by using the commnand "*?".

\-
~~
It has channel ID as parameter. No output.

Disable a channel.

Example:

| send:
| ``-0``

You can consult the new state by using the commnand "*?".

B
~~
It has channel ID as parameter.

Outputs status for a channel. The status string is in the form
"*temperature,setpoint,output_value*"

Example:

| send:
| ``B0``
| receive:
| ``:B0,140,82,0#``

Seems the values are multiplied by 10. NOTE: I don't know what "output_value"
means.

This command does not support reconfigure parameters.

D
~~
It has channel ID as parameter.

Outputs the configuration values. These values are showed as a string of the form
"*min_output,max_output,is_autostart,temp_offset,setpoint_offset,Kp,Ki,Kp*"

Example:

| send:
| ``D0``
| receive:
| ``:D0,0,255,0,0,0,100,3,0#``

This command support reconfigure parameters. For do this you must add to the
command a string similar to the output of this command, but with the desired values.

Example for reconfiguration:

| send:
| ``D0,0,255,0,0,0,101,4,1``

The reconfiguration command does not has output. You can consult the new values
after setting them.

Save new values in the eeprom memory seems don't works.

F
~~
It has channel ID as parameter.

Outputs the sensor (NTC) configuration values. These values are showed as a string
of the form "*nominal_temp,resistor_value,bcoefficient_value,>nominal_value*"

Example:

| send:
| ``F0``
| receive:
| ``:F0,250,-31072,3950,10000#``

This command does not support reconfigure parameters.

A
~~
It has no parameters.

Outputs status for ambient sensor. The status string is in the form
"*temperature,humidity,dew_point*"

Example:

| send:
| ``A``
| receive:
| ``:A150,650,84#``

Seems the values are multiplied by 10.

This command does not support reconfigure parameters.

G
~~
It has no parameters.

Outputs the ambient configuration. These values are showed as a string of the form
"*temp_offset,rh_offset*"

Example:

| send:
| ``G``
| receive:
| ``:G0,0#``

This command support reconfigure parameters. For do this you must add to the
command a string similar to the output of this command, but with the desired values.

Example for reconfiguration:

| send:
| ``G1,1``

The reconfiguration command does not has output. You can consult the new values
after setting them.

Save new values in the eeprom memory seems don't works.

Commands not yet implemented
============================

C
~~

I
~~

E
~~

W
~~

Use serial command from linux terminal
======================================
First, prepare the tty port for use with arduino (assuming your device is in
/dev/ttyUSB0) the speed arduheater seems to use is 57600 bauds:

::

$ stty -F /dev/ttyUSB0 cs8 57600 ignbrk -brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke noflsh -ixon -crtscts

Now you can open two theminal. In one of them we will see the outputs with the
command:

::

$ cat /dev/ttyUSB0

and in the other terminal we can send commands (XX in the example) by using:

::

$ echo  "XX" > /dev/ttyUSB0

