ArduheaterBT
============

"ArduheaterBT" is an Android application that allow you to analise, show and
set parameters of the ["Arduheater" project](https://github.com/jbrazio/arduheater),
by jbrazio.

"ArduheaterBT" connect with the arduheater strip controller by use a bluetooth
adapter, such as HC-06.

"ArduheaterBT" is released under MIT licence.

Due the technical characteristics of the integration of Android Studio
projects with git repositories, please find the source code in a separate
[git project](https://gitlab.com/monje/arduheaterbt).

