ArduHeaterLite PCB
==================

PCB for the lite version of ArduHeater:
https://github.com/jbrazio/arduheater

Redesigned PCB with a smaller footprint and a connector for a Bluetooth module. 


## BOM list

 * RESISTOR 10K 1/4W PRECISION (x5) (R_ref1, R_ref2, R_ref3, R_ref4, R_sens1)

 * RESISTOR 56K 1/4W (x5) (R_led0, R_led1, R_led2, R_led3, R_led4)

 * RESISTOR 1K 1/4W (x4) (R_g1, R_g2, R_g3, R_g4)

 * CAPACITOR CERAMIC 100nF D4.3mm P5mm (x4) (C1, C2, C3, C4)

     * https://es.aliexpress.com/item/32973259342.html

 * CAPACITOR ELECTROLITIC 10uF D5mm P2.5mm (x1) (C0)

     * https://es.aliexpress.com/item/4000685311373.html

 * MOSFET IRL3705 (x4) (MOSFET1, MOSFET2, MOSFET3, MOSFET4)

     * https://es.aliexpress.com/item/4000799911071.html

 * DIODE 1N4007 (x4) (D1, D2, D3, D4)

     * https://es.aliexpress.com/item/32664545131.html

 * DHT22 SENSOR (1x) (TH1)

     * https://es.aliexpress.com/item/32884816378.html

 * PIN HEADER 2.54mm VERTICAL (MALE) 2P (x5) (LED_0, LED_1, LED_2, LED_3, LED_4)

     * https://es.aliexpress.com/item/4001187613681.html

 * PIN HEADER 2.54mm VERTICAL (MALE) 4P (x4) (HEATER_1, HEATER_2, HEATER_3, HEATER_4)

     * https://es.aliexpress.com/item/4001187613681.html

 * PIN HEADER 2.54mm HORIZONTAL (FEMALE) 4P (x1) (HC6)

     * https://es.aliexpress.com/item/32980998451.html

 * PIN HEADER 2.54mm VERTICAL (FEMALE) 15P (x2) (for arduino slot)

     * https://es.aliexpress.com/item/4001168161150.html

 * TERMINAL BLOCK 3.5mm 2P (J1)

     * https://es.aliexpress.com/item/32981258851.html

 * ARDUINO NANO (x1)

     * https://es.aliexpress.com/item/32341832857.html

NOTE: links provided only as reference.

![circuit buikt](media/circuit_built.jpg)

