EESchema Schematic File Version 4
LIBS:ArduHeaterLite-cache
EELAYER 30 0
EELAYER END
$Descr User 5197 4000
encoding utf-8
Sheet 2 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R_ref1
U 1 1 5DBD96F7
P 1500 1150
AR Path="/5DBC5600/5DBD96F7" Ref="R_ref1"  Part="1" 
AR Path="/5DBE146A/5DBD96F7" Ref="R_ref2"  Part="1" 
AR Path="/5DBE2140/5DBD96F7" Ref="R_ref3"  Part="1" 
AR Path="/5DBE2301/5DBD96F7" Ref="R_ref4"  Part="1" 
F 0 "R_ref4" V 1293 1150 50  0000 C CNN
F 1 "10K" V 1384 1150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1430 1150 50  0001 C CNN
F 3 "~" H 1500 1150 50  0001 C CNN
	1    1500 1150
	0    1    1    0   
$EndComp
Text HLabel 1200 900  0    50   UnSpc ~ 0
analog
Text HLabel 1200 1150 0    50   UnSpc ~ 0
aref
$Comp
L Connector_Generic:Conn_01x04 HEATER_1
U 1 1 5DC0565F
P 2250 1150
AR Path="/5DBC5600/5DC0565F" Ref="HEATER_1"  Part="1" 
AR Path="/5DBE2140/5DC0565F" Ref="HEATER_3"  Part="1" 
AR Path="/5DBE2301/5DC0565F" Ref="HEATER_4"  Part="1" 
AR Path="/5DBE146A/5DC0565F" Ref="HEATER_2"  Part="1" 
F 0 "HEATER_4" V 2350 950 50  0000 L CNN
F 1 "Conn_01x04" H 2330 1051 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 2250 1150 50  0001 C CNN
F 3 "~" H 2250 1150 50  0001 C CNN
	1    2250 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 1150 1200 1150
Wire Wire Line
	1700 1150 1700 900 
Wire Wire Line
	1700 900  1200 900 
Connection ~ 1700 1150
Wire Wire Line
	1700 1150 1650 1150
$Comp
L power:Earth #PWR0106
U 1 1 5DC15467
P 2600 2100
AR Path="/5DBC5600/5DC15467" Ref="#PWR0106"  Part="1" 
AR Path="/5DBE2140/5DC15467" Ref="#PWR0112"  Part="1" 
AR Path="/5DBE2301/5DC15467" Ref="#PWR0115"  Part="1" 
AR Path="/5DBE146A/5DC15467" Ref="#PWR0109"  Part="1" 
F 0 "#PWR0115" H 2600 1850 50  0001 C CNN
F 1 "Earth" H 2600 1950 50  0001 C CNN
F 2 "" H 2600 2100 50  0001 C CNN
F 3 "~" H 2600 2100 50  0001 C CNN
	1    2600 2100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R_g1
U 1 1 5DC15F06
P 2100 1850
AR Path="/5DBC5600/5DC15F06" Ref="R_g1"  Part="1" 
AR Path="/5DBE2140/5DC15F06" Ref="R_g3"  Part="1" 
AR Path="/5DBE2301/5DC15F06" Ref="R_g4"  Part="1" 
AR Path="/5DBE146A/5DC15F06" Ref="R_g2"  Part="1" 
F 0 "R_g4" V 2307 1850 50  0000 C CNN
F 1 "1K" V 2216 1850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2030 1850 50  0001 C CNN
F 3 "~" H 2100 1850 50  0001 C CNN
	1    2100 1850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2250 1850 2300 1850
Text HLabel 1850 1850 0    50   Input ~ 0
digital
Wire Wire Line
	1950 1850 1850 1850
$Comp
L Device:R R_led1
U 1 1 5DBCBBD4
P 3000 1450
AR Path="/5DBC5600/5DBCBBD4" Ref="R_led1"  Part="1" 
AR Path="/5DBE2140/5DBCBBD4" Ref="R_led3"  Part="1" 
AR Path="/5DBE146A/5DBCBBD4" Ref="R_led2"  Part="1" 
AR Path="/5DBE2301/5DBCBBD4" Ref="R_led4"  Part="1" 
F 0 "R_led4" V 3100 1450 50  0000 C CNN
F 1 "56K" V 2900 1300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2930 1450 50  0001 C CNN
F 3 "~" H 3000 1450 50  0001 C CNN
	1    3000 1450
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x02 LED_1
U 1 1 5DBCF23E
P 3350 1150
AR Path="/5DBC5600/5DBCF23E" Ref="LED_1"  Part="1" 
AR Path="/5DBE2140/5DBCF23E" Ref="LED_3"  Part="1" 
AR Path="/5DBE146A/5DBCF23E" Ref="LED_2"  Part="1" 
AR Path="/5DBE2301/5DBCF23E" Ref="LED_4"  Part="1" 
F 0 "LED_4" V 3450 1000 50  0000 L CNN
F 1 "+" H 3300 1250 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3350 1150 50  0001 C CNN
F 3 "~" H 3350 1150 50  0001 C CNN
	1    3350 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 2050 2600 2100
$Comp
L Diode:1N4007 D1
U 1 1 5DC47D2D
P 2600 1200
AR Path="/5DBC5600/5DC47D2D" Ref="D1"  Part="1" 
AR Path="/5DBE2301/5DC47D2D" Ref="D4"  Part="1" 
AR Path="/5DBE146A/5DC47D2D" Ref="D2"  Part="1" 
AR Path="/5DBE2140/5DC47D2D" Ref="D3"  Part="1" 
F 0 "D4" V 2554 1279 50  0000 L CNN
F 1 "1N4007" H 2500 1100 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P7.62mm_Horizontal" H 2600 1025 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 2600 1200 50  0001 C CNN
	1    2600 1200
	0    1    1    0   
$EndComp
$Comp
L Device:CP1 C1
U 1 1 5DC4BD36
P 1700 1300
AR Path="/5DBC5600/5DC4BD36" Ref="C1"  Part="1" 
AR Path="/5DBE2301/5DC4BD36" Ref="C4"  Part="1" 
AR Path="/5DBE146A/5DC4BD36" Ref="C2"  Part="1" 
AR Path="/5DBE2140/5DC4BD36" Ref="C3"  Part="1" 
F 0 "C4" H 1550 1150 50  0000 L CNN
F 1 "100n" V 1550 1200 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D4.3mm_W1.9mm_P5.00mm" H 1700 1300 50  0001 C CNN
F 3 "~" H 1700 1300 50  0001 C CNN
	1    1700 1300
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0118
U 1 1 5DC4E525
P 1700 1450
AR Path="/5DBC5600/5DC4E525" Ref="#PWR0118"  Part="1" 
AR Path="/5DBE2301/5DC4E525" Ref="#PWR0121"  Part="1" 
AR Path="/5DBE146A/5DC4E525" Ref="#PWR0119"  Part="1" 
AR Path="/5DBE2140/5DC4E525" Ref="#PWR0120"  Part="1" 
F 0 "#PWR0121" H 1700 1200 50  0001 C CNN
F 1 "Earth" H 1700 1300 50  0001 C CNN
F 2 "" H 1700 1450 50  0001 C CNN
F 3 "~" H 1700 1450 50  0001 C CNN
	1    1700 1450
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0105
U 1 1 5DC0DC1F
P 2600 800
AR Path="/5DBC5600/5DC0DC1F" Ref="#PWR0105"  Part="1" 
AR Path="/5DBE2140/5DC0DC1F" Ref="#PWR0111"  Part="1" 
AR Path="/5DBE2301/5DC0DC1F" Ref="#PWR0114"  Part="1" 
AR Path="/5DBE146A/5DC0DC1F" Ref="#PWR0108"  Part="1" 
F 0 "#PWR0114" H 2600 650 50  0001 C CNN
F 1 "+12V" H 2615 973 50  0000 C CNN
F 2 "" H 2600 800 50  0001 C CNN
F 3 "" H 2600 800 50  0001 C CNN
	1    2600 800 
	-1   0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0104
U 1 1 5DBD9E7E
P 2000 1250
AR Path="/5DBC5600/5DBD9E7E" Ref="#PWR0104"  Part="1" 
AR Path="/5DBE146A/5DBD9E7E" Ref="#PWR0107"  Part="1" 
AR Path="/5DBE2140/5DBD9E7E" Ref="#PWR0110"  Part="1" 
AR Path="/5DBE2301/5DBD9E7E" Ref="#PWR0113"  Part="1" 
F 0 "#PWR0113" H 2000 1000 50  0001 C CNN
F 1 "Earth" H 2000 1100 50  0001 C CNN
F 2 "" H 2000 1250 50  0001 C CNN
F 3 "~" H 2000 1250 50  0001 C CNN
	1    2000 1250
	0    1    1    0   
$EndComp
$Comp
L Transistor_FET:IRF3205 MOSFET1
U 1 1 5DBDC6C4
P 2500 1850
AR Path="/5DBC5600/5DBDC6C4" Ref="MOSFET1"  Part="1" 
AR Path="/5DBE146A/5DBDC6C4" Ref="MOSFET2"  Part="1" 
AR Path="/5DBE2140/5DBDC6C4" Ref="MOSFET3"  Part="1" 
AR Path="/5DBE2301/5DBDC6C4" Ref="MOSFET4"  Part="1" 
F 0 "MOSFET4" H 2706 1896 50  0000 L CNN
F 1 "IRL3705" H 2706 1805 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 2750 1775 50  0001 L CIN
F 3 "http://www.irf.com/product-info/datasheets/data/irf3205.pdf" H 2500 1850 50  0001 L CNN
	1    2500 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 1250 2050 1250
Wire Wire Line
	2050 1050 2050 950 
Wire Wire Line
	2050 950  2600 950 
Wire Wire Line
	2600 950  2600 1050
Wire Wire Line
	2050 1350 2050 1450
Wire Wire Line
	2050 1450 2600 1450
Wire Wire Line
	2600 1450 2600 1350
Wire Wire Line
	1700 1150 2050 1150
Wire Wire Line
	2600 1450 2600 1650
Connection ~ 2600 1450
Wire Wire Line
	2600 950  3150 950 
Wire Wire Line
	3150 950  3150 1150
Connection ~ 2600 950 
Wire Wire Line
	2600 800  2600 950 
Wire Wire Line
	2600 1450 2850 1450
Wire Wire Line
	3150 1450 3150 1250
$EndSCHEMATC
