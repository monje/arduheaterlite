EESchema Schematic File Version 4
LIBS:ArduHeaterLite-cache
EELAYER 30 0
EELAYER END
$Descr User 6299 5906
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ArduHeaterLite-rescue:Arduino_Nano_v3.x-MCU_Module A1
U 1 1 5DB885F9
P 2600 2500
F 0 "A1" H 2600 1411 50  0000 C CNN
F 1 "Arduino_Nano_v3.x" H 2600 1320 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 2750 1550 50  0001 L CNN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 2600 1500 50  0001 C CNN
	1    2600 2500
	0    -1   -1   0   
$EndComp
$Sheet
S 4200 1200 850  450 
U 5DBC5600
F0 "heater_module-1" 50
F1 "heater_module.sch" 50
F2 "analog" U L 4200 1400 50 
F3 "aref" U R 5050 1250 50 
F4 "digital" I L 4200 1550 50 
$EndSheet
$Sheet
S 4200 1900 850  450 
U 5DBE146A
F0 "heater_module-2" 50
F1 "heater_module.sch" 50
F2 "analog" U L 4200 2100 50 
F3 "aref" U R 5050 1950 50 
F4 "digital" I L 4200 2250 50 
$EndSheet
$Sheet
S 4200 2600 850  450 
U 5DBE2140
F0 "heater_module-3" 50
F1 "heater_module.sch" 50
F2 "analog" U L 4200 2800 50 
F3 "aref" U R 5050 2650 50 
F4 "digital" I L 4200 2950 50 
$EndSheet
$Sheet
S 4200 3300 850  450 
U 5DBE2301
F0 "heater_module-4" 50
F1 "heater_module.sch" 50
F2 "analog" U L 4200 3500 50 
F3 "aref" U R 5050 3350 50 
F4 "digital" I L 4200 3650 50 
$EndSheet
$Comp
L power:+12V #PWR0101
U 1 1 5DBC73AC
P 1550 2600
F 0 "#PWR0101" H 1550 2450 50  0001 C CNN
F 1 "+12V" V 1565 2728 50  0000 L CNN
F 2 "" H 1550 2600 50  0001 C CNN
F 3 "" H 1550 2600 50  0001 C CNN
	1    1550 2600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1600 2600 1550 2600
$Comp
L Device:CP1 C0
U 1 1 5DBC8D9E
P 2000 1800
F 0 "C0" H 1885 1754 50  0000 R CNN
F 1 "10uF" H 1885 1845 50  0000 R CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 2000 1800 50  0001 C CNN
F 3 "~" H 2000 1800 50  0001 C CNN
	1    2000 1800
	-1   0    0    1   
$EndComp
Wire Wire Line
	5350 3350 5350 2650
Wire Wire Line
	5350 900  2400 900 
Wire Wire Line
	2400 900  2400 2000
Wire Wire Line
	5050 3350 5350 3350
Wire Wire Line
	5050 2650 5350 2650
Connection ~ 5350 2650
Wire Wire Line
	5350 2650 5350 1950
Wire Wire Line
	5050 1950 5350 1950
Connection ~ 5350 1950
Wire Wire Line
	5350 1950 5350 1250
Wire Wire Line
	5050 1250 5350 1250
Connection ~ 5350 1250
Wire Wire Line
	5350 1250 5350 900 
Wire Wire Line
	4200 1400 2600 1400
Wire Wire Line
	2600 1400 2600 2000
Wire Wire Line
	4200 2100 4150 2100
Wire Wire Line
	4150 2100 4150 1650
Wire Wire Line
	4150 1650 2700 1650
Wire Wire Line
	2700 1650 2700 2000
Wire Wire Line
	4200 2800 4100 2800
Wire Wire Line
	4100 2800 4100 1750
Wire Wire Line
	4100 1750 2800 1750
Wire Wire Line
	2800 1750 2800 2000
Wire Wire Line
	4200 3500 4050 3500
Wire Wire Line
	4050 3500 4050 1850
Wire Wire Line
	4050 1850 2900 1850
Wire Wire Line
	2900 1850 2900 2000
Wire Wire Line
	4200 1550 3650 1550
Wire Wire Line
	3650 1550 3650 2300
Wire Wire Line
	3650 2300 3100 2300
Wire Wire Line
	3100 2300 3100 3000
Wire Wire Line
	4200 2250 2600 2250
Wire Wire Line
	2600 2250 2600 3000
Wire Wire Line
	4200 2950 3650 2950
Wire Wire Line
	3650 2950 3650 3200
Wire Wire Line
	3650 3200 2500 3200
Wire Wire Line
	2500 3200 2500 3000
Wire Wire Line
	4200 3650 2300 3650
Wire Wire Line
	2000 2000 2000 1950
$Comp
L power:Earth #PWR0102
U 1 1 5DBD5A2C
P 2000 1600
F 0 "#PWR0102" H 2000 1350 50  0001 C CNN
F 1 "Earth" H 2000 1450 50  0001 C CNN
F 2 "" H 2000 1600 50  0001 C CNN
F 3 "~" H 2000 1600 50  0001 C CNN
	1    2000 1600
	-1   0    0    1   
$EndComp
Wire Wire Line
	2000 1600 2000 1650
$Comp
L power:Earth #PWR0103
U 1 1 5DBE7604
P 1500 2000
F 0 "#PWR0103" H 1500 1750 50  0001 C CNN
F 1 "Earth" H 1500 1850 50  0001 C CNN
F 2 "" H 1500 2000 50  0001 C CNN
F 3 "~" H 1500 2000 50  0001 C CNN
	1    1500 2000
	-1   0    0    1   
$EndComp
$Comp
L Device:R R_sens1
U 1 1 5DBE9BFE
P 900 3050
F 0 "R_sens1" V 1000 2900 50  0000 L CNN
F 1 "10K" H 700 3100 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 830 3050 50  0001 C CNN
F 3 "~" H 900 3050 50  0001 C CNN
	1    900  3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 3000 2200 3200
$Comp
L Connector:Screw_Terminal_01x02 J1
U 1 1 5DBD80C6
P 1200 750
F 0 "J1" V 1164 562 50  0000 R CNN
F 1 "Screw_Terminal_01x02" V 1073 562 50  0000 R CNN
F 2 "TerminalBlock_4Ucon:TerminalBlock_4Ucon_1x02_P3.50mm_Horizontal" H 1200 750 50  0001 C CNN
F 3 "~" H 1200 750 50  0001 C CNN
	1    1200 750 
	0    -1   -1   0   
$EndComp
$Comp
L power:+12V #PWR0116
U 1 1 5DBD8DFD
P 800 1250
F 0 "#PWR0116" H 800 1100 50  0001 C CNN
F 1 "+12V" H 815 1423 50  0000 C CNN
F 2 "" H 800 1250 50  0001 C CNN
F 3 "" H 800 1250 50  0001 C CNN
	1    800  1250
	-1   0    0    1   
$EndComp
$Comp
L power:Earth #PWR0117
U 1 1 5DBD9AB8
P 1700 1250
F 0 "#PWR0117" H 1700 1000 50  0001 C CNN
F 1 "Earth" H 1700 1100 50  0001 C CNN
F 2 "" H 1700 1250 50  0001 C CNN
F 3 "~" H 1700 1250 50  0001 C CNN
	1    1700 1250
	1    0    0    -1  
$EndComp
$Comp
L MyLibrary:DHT22_Temperature_Humidity TH1
U 1 1 5DC53513
P 1500 2150
F 0 "TH1" V 1937 2567 60  0001 C CNN
F 1 "DHT22_Temperature_Humidity" H 1550 3100 60  0000 C CNN
F 2 "MyLibrary:DHT22_Temperature_Humidity" H 1500 2150 60  0001 C CNN
F 3 "" H 1500 2150 60  0000 C CNN
	1    1500 2150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2300 3000 2300 3650
Wire Wire Line
	1500 2300 1600 2300
Wire Wire Line
	1500 2200 1600 2200
Wire Wire Line
	1600 2200 1600 1800
Wire Wire Line
	1600 1800 600  1800
Wire Wire Line
	600  1800 600  3200
Wire Wire Line
	600  3200 900  3200
Connection ~ 900  3200
Wire Wire Line
	900  3200 2200 3200
Wire Wire Line
	1500 2300 1500 2450
Wire Wire Line
	1500 2450 900  2450
Wire Wire Line
	900  2450 900  2900
Connection ~ 1500 2300
$Comp
L Connector_Generic:Conn_02x01 LED_0
U 1 1 5DCA9CF3
P 1000 1250
F 0 "LED_0" H 1050 1467 50  0000 C CNN
F 1 "+" H 850 1350 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1000 1250 50  0001 C CNN
F 3 "~" H 1000 1250 50  0001 C CNN
	1    1000 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R_led0
U 1 1 5DCAA609
P 1450 1250
F 0 "R_led0" V 1243 1250 50  0000 C CNN
F 1 "56K" V 1334 1250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1380 1250 50  0001 C CNN
F 3 "~" H 1450 1250 50  0001 C CNN
	1    1450 1250
	0    1    1    0   
$EndComp
Wire Wire Line
	1200 950  800  950 
Wire Wire Line
	800  950  800  1250
Connection ~ 800  1250
Wire Wire Line
	1300 950  1700 950 
Wire Wire Line
	1700 950  1700 1250
Wire Wire Line
	1700 1250 1600 1250
Connection ~ 1700 1250
$Comp
L power:Earth #PWR0122
U 1 1 5E090EA4
P 3650 2400
F 0 "#PWR0122" H 3650 2150 50  0001 C CNN
F 1 "Earth" H 3650 2250 50  0001 C CNN
F 2 "" H 3650 2400 50  0001 C CNN
F 3 "~" H 3650 2400 50  0001 C CNN
	1    3650 2400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3600 2400 3650 2400
$Comp
L Connector_Generic:Conn_01x04 HC6
U 1 1 5E0B8DC0
P 1250 2700
F 0 "HC6" V 1214 2412 50  0000 R CNN
F 1 "Conn_01x04" V 1123 2412 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Horizontal" H 1250 2700 50  0001 C CNN
F 3 "~" H 1250 2700 50  0001 C CNN
	1    1250 2700
	0    1    -1   0   
$EndComp
Wire Wire Line
	1050 2900 900  2900
Connection ~ 900  2900
$Comp
L power:Earth #PWR0123
U 1 1 5E0CD9D3
P 1150 3000
F 0 "#PWR0123" H 1150 2750 50  0001 C CNN
F 1 "Earth" H 1150 2850 50  0001 C CNN
F 2 "" H 1150 3000 50  0001 C CNN
F 3 "~" H 1150 3000 50  0001 C CNN
	1    1150 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 2900 1150 3000
Wire Wire Line
	1250 2900 1250 3150
Wire Wire Line
	1250 3150 2000 3150
Wire Wire Line
	2000 3150 2000 3000
Wire Wire Line
	1350 2900 1350 3050
Wire Wire Line
	1350 3050 2100 3050
Wire Wire Line
	2100 3050 2100 3000
$EndSCHEMATC
